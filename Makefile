REBAR = $(shell pwd)/rebar3
ELVIS = $(shell pwd)/elvis
APP = howl
include config.mk

.PHONY: rel stagedevrel package version all tree

all: compile version_header

include fifo.mk

version:
	@echo '$(shell git symbolic-ref HEAD 2> /dev/null | cut -b 12-)-$(shell git log --pretty=format:"%h, %ad" -1)' > howl.version

version_header: version
	@echo "-define(VERSION, <<\"$(shell cat howl.version)\">>)." > apps/howl/include/howl_version.hrl

clean:
	$(REBAR) clean
	$(MAKE) -C rel/pkg clean

long-test:
	$(REBAR) as eqc,long eunit
